#   Archify - alternates the capitalization of text
#   Copyright (C) 2018 Cameron Himes
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# get input
ans = input("Enter text: ")

# make place to store output
output = ""
# make flag for loop
isUpper = True

# iterate over every character
for pos in range(len(ans)):
    # is it a letter
    if ans[pos].isalpha():
        # add the formatted character and toggle the flag
        if isUpper:
            output += ans[pos].upper()
            isUpper = False
        else:
            output += ans[pos].lower()
            isUpper = True
    else:
        # character is not a letter, add it and do nothing special
        output += ans[pos]

# shift down a line and print result
print()
print(output)