# Archify

This program changes the capitalization of text to match the "BTW I USE ARCH" memes.

![screenshot](Screenshot.png)

## Usage

`python3 src/Archify.py`
